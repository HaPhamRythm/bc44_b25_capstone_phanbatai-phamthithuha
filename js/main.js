const BASE_URL = "https://64470f6a50c25337441dbd07.mockapi.io/pr";
function fetchProduct() {
  turnOnLoading();
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      console.log(res.data);
      renderProduct(res.data.reverse());
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(err);
    });
}

fetchProduct();

// delete

function deletePr(id) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOffLoading();
      console.log(res);
      fetchProduct();
      Toastify({
        text: "Successfully deleted",
        className: "info",
        style: {
          background: "linear-gradient(to right, #00b09b, #96c93d)",
        },
      }).showToast();
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(err);
    });
}

// add new products

function addProduct() {
  turnOnLoading();

  var pr = takeInfoFromForm();

  var isValid =
    checkBlank("spanPrice", pr.price) &
    checkPrice("spanPrice", pr.price) &
    checkBlank("spanName", pr.name) &
    checkType("spanType", pr.typeText);
  if (isValid) {
    axios({
      url: BASE_URL,
      method: "POST",
      data: takeInfoFromForm(),
    })
      .then(function (res) {
        turnOffLoading();
        fetchProduct();
        console.log(res);
      })
      .catch(function (err) {
        turnOffLoading();
        console.log(err);
      });
  }
}

// edit & update

function editPr(id) {
  document.getElementById("txtPrID").disabled = true;
  selectedID = id;
  turnOnLoading();

  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then(function (res) {
      turnOffLoading();
      showInfoToForm(res.data);
      console.log(res);
    })
    .catch(function (err) {
      turnOffLoading();
      console.log(err);
    });
}

function updateProduct() {
  var editedProduct = takeInfoFromForm();
  var isValid =
    checkBlank("spanPrice", editedProduct.price) &
    checkPrice("spanPrice", editedProduct.price) &
    checkBlank("spanName", editedProduct.name) &
    checkType("spanType", editedProduct.typeText);
  console.log(isValid);
  if (isValid) {
    axios({
      url: `${BASE_URL}/${selectedID}`,
      method: "PUT",
      data: takeInfoFromForm(),
    })
      .then(function (res) {
        fetchProduct();
        console.log(res);
      })
      .catch(function (err) {
        console.log(err);
      });
  }
}

// find the product by name

async function getAllProduct() {
  return await axios
    .get("https://64470f6a50c25337441dbd07.mockapi.io/pr")
    .then(function (res) {
      return res.data;
    });
}

console.log(getAllProduct());
async function search() {
  searchedName = document.getElementById("txtSearch").value;
  let prList = await getAllProduct();

  var newArray = prList.filter(function (item) {
    return item.name.toLowerCase().includes(searchedName.toLowerCase());
  });

  renderProduct(newArray);
  if (searchedName == "") {
    renderProduct(prList);
  }
}

// sort the order of price

async function sortPrice() {
  let prList2 = await getAllProduct();

  var descending = document
    .getElementById("sortPr")
    .classList.toggle("descending");

  var sortDescending = prList2.sort((a, b) => a.price * 1 - b.price * 1);
  if (descending == false) {
    renderProduct(sortDescending);
  }
  if (descending == true) {
    renderProduct(sortDescending.reverse());
  }
}

// reset

function resetForm() {
  document.getElementById("formQLSV").reset();
}
